# leakbot
[![PEP8](https://img.shields.io/badge/code%20style-pep8-green.svg)](https://www.python.org/dev/peps/pep-0008/)
[![build status](https://gitgud.io/sug/leakbot/badges/master/build.svg)](https://gitgud.io/sug/leakbot/commits/master)

> https://sug.rocks/leaks.html

This is the code for the leakbot. It scans CN's video servers to find screenshots and a short video preview for Steven Universe episodes.  
Everything this bot finds is on a public-facing server. The goal is to find those before they are put on the website, because we can't wait 4 days.


## Install
You **must** clone this as a subproject of [sug/website](https://gitgud.io/sug/website) and get all the depedencies from the `requirements.txt` there.  
Copy the `config.ini.example` to `config.ini` and edit it.

You can now add a crontab for it. `crontab -e`
```
@hourly cd /path/to/sug/leakbot ; /usr/local/bin/pipenv run python leakbot.py quiet > cron.log 2>&1
```
_Obviously change `/path/to/sug/leakbot` to the correct path._


## Config
Value          | Description
---------------|-------------
`pushapi`      | The API key from Pushbullet to send notifications to you and your subscribers.
`idstart`      | The contentId from CN's video servers where the bot will start his scan. (**[!]** The bot will go back a bit (see `gobackabit`).)
`howmany`      | From `idstart`, how many videoIDs will the bot go through (+`gobackabit` automatically).
`gobackabit`   | The bot may need to check back before the latest thing it found to be sure it didn't miss something important.
`filter`       | Comma-separated (without spaces) list of IDs to ignore.
`lastestanyid` | Saved by the bot itself. The ID of the last content it found, unrelated or not to our search.
`lastsuid`     | Saved by the bot itself. The last ID from something SU related.

---

## How it works _(rougly)_

First, you'll need a contentId.
##### Where can I find a contentId to start?
- Visit <http://www.cartoonnetwork.com/video/> and open the first episode from the "latest episodes" module
- Open the webpage source code (<kbd>CTRL+U</kbd>) and search for `_cnglobal.cvpVideoId`
- Congrats, you found it! It's the value of this variable

Here's one for example: `894526`.

##### Get the data
- Take `http://www.cartoonnetwork.com/cnservice/cartoonsvc/content/xml/getContentById.do?contentId=`
- Add your ID at the end
- Visit it
- Boom! Lots of things!

If this page exists, that means we can get (if available):
- The CareID (`<AssetId />`)
- Proper episode title (`<episodeTitle />`)
- Synopsis (`<VideoSynopsis />`)
- Air date (`<lastAirDate />` contains the date AND time, PT timezone)
- Screenshots (`<Video_Thumb_1280_720 />` for 720p)
- (And more)

**BUT**, we check two things:
- It _must_ be a `<TVE_Video_Grouper_1 />` node at the root. We don't want to get `<Full_Episode />` or anything else.
- We don't care about the spanish version. So if `<metaKeywords />` contains `español`, we skip it.

##### Getting medias
The .xml contains the 720p screenshots in `<Video_Thumb_1280_720 />`, which contains a `<srcUrl>` that looks like `Images/i[id?]/[file]`.  
Prepend `http://i.cdn.turner.com/v5cache/CARTOON/site/` and here we go, a screenshot! We download them and save their filename for display later.  
Note that there might be two or three `<Video_Thumb_1280_720 />` with the same URL.

For the video preview, the files are stored on the same folder: `http://ht.cdn.turner.com/toon/big/preview/tve/`.  
But there is two possibilities for the video filename. (I guess it is now only _one_, but fuck.) It's either `[CAREID]_B13R1.mp4` or `[CAREID]_3500kbps_1280x720.mp4`.  
We check if it doesn't 404 and save it.

##### Generating pages
Well, we store each values in variables, use jinja2 templates and we dump the html/xml/json to the correct folder.  
Look at the python code, starting the `print('\nLoading templates')` line regarding that.
