import sys
import os
import json
import pytz
import shutil
import crayons
import subprocess
import configparser
import urllib.error
import urllib.parse
import urllib.request
import multiprocessing
import better_exceptions

from pytz import timezone
from datetime import datetime
from bs4 import BeautifulSoup
from PIL import Image, ImageDraw
from pushbullet import Pushbullet
from jinja2 import Environment, FileSystemLoader

better_exceptions.MAX_LENGTH = None
THIS_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = os.path.join(THIS_DIR, os.pardir)
PUBLIC_DIR = os.path.join(ROOT_DIR, 'public')
LEAK_DIR = os.path.join(PUBLIC_DIR, 'leaks')

# Loading our config
config = configparser.ConfigParser()
config.read(os.path.join(THIS_DIR, 'config.ini'))

# Init values
gobackabit = int(config['DEFAULT']['gobackabit'])
id_start = int(config['DEFAULT']['idstart']) - gobackabit
how_many = int(config['DEFAULT']['howmany']) + gobackabit
leaks = []
otherleaks = []
push_queue = []
founds = 0
last_found = 'None'

# Load Pushbullet if there's a key
try:
    if config['DEFAULT']['pushapi']:
        pb = Pushbullet(config['DEFAULT']['pushapi'])
    else:
        pb = False
except:
    pb = False


def update_progress(done):
    # Display current progress
    amt_done = (done / how_many)
    if len(sys.argv) <= 1 or (len(sys.argv) > 1 and (sys.argv[1] != 'quiet')):  # and sys.argv[1] != 'remote')):
        # From comments of this answer: http://stackoverflow.com/a/3173331
        pbar = crayons.green('{:50s}'.format('#' * int(amt_done * 50)))  # makes a progress bar
        prog = crayons.green('{:.1f}%'.format(amt_done * 100))  # displays progress percentage
        sys.stdout.write('\r[{}] {} ({}) {} {}'.format(
            pbar, prog, crayons.blue(str(done) + '/' + str(how_many)),
            crayons.blue('Parsed: ' + str(founds)), crayons.magenta('Last: ' + last_found)
        ))


def get_leaks():
    # Main function
    global id_start, how_many, leaks, push_queue, founds, last_found

    # Print current date and time for debug
    print(crayons.blue(datetime.now()))

    print(crayons.blue('Scanning from %d to %d' % (id_start, (id_start + how_many))))

    for i in range(id_start, (id_start + how_many)):
        # If it's in the filter list
        if str(i) in config['DEFAULT']['filter'].split(','):
            continue

        # Show progress and let's check that URL
        update_progress(i - id_start + 1)
        url = 'http://www.cartoonnetwork.com/cnservice/cartoonsvc/content/xml/getContentById.do?contentId=' + str(i)

        # If opening the page fail, just continue
        try:
            page_request = urllib.request.urlopen(url)
            xml = page_request.read().decode('utf-8')
            soup = BeautifulSoup(xml, 'lxml-xml')
            if soup.TVE_Video_Grouper_1.has_attr('contentId'):
                # If it's from tve_video_grouper_1 and has a contentid
                meta = soup.TVE_Video_Grouper_1
            else:
                # Not a tve_video_grouper_1? Nope.
                print(crayons.yellow('\nParsed: ' + str(i)))
                print(crayons.yellow('Not tve_grouper_1'))
                continue
        except AttributeError:
            continue
        except:
            print('Error with ' + str(i))
            continue

        last_found = str(i)
        update_progress(i - id_start + 1)

        # If the CAREID is in the file and there's a show name
        if meta.AssetId.string is not None and meta.SeriesName.string is not None:
            config['DEFAULT']['idstart'] = str(i)

            try:
                air_date = meta.lastAirDate.string
                if air_date is None:
                    # If there isn't any airing date, ignore that useless piece of junk
                    continue

                careid = meta.AssetId.string
                show = meta.SeriesName.string
                title = meta.episodeTitle.string
                desc = meta.VideoSynopsis.string
                if 'español' in meta.metaKeywords.string.lower():
                    # Skip if there's the spanish keyword
                    print(crayons.yellow('Spanish episode, skip it'))
                    continue
            except:
                # If there isn't any metadata available, skip it
                if pb:
                    pb.push_note('LEAKBOT has a problem!',
                                 'Can\'t find what to do with meta (ID: ' + str(i) + ')')
                else:
                    print(crayons.red('Can\'t find what to do with meta'))
                continue

            # Hey, so we found something!
            founds += 1

            # If this id is higher than the last one found, notify people
            if config['DEFAULT']['lastsuid'] < str(i):
                if show == 'Steven Universe':
                    push_queue.append(title)
                    config['DEFAULT']['lastsuid'] = str(i)

            # Just to test if we should create a new key or not
            try:
                config[str(i)]['date']
            except:
                config.add_section(str(i))
                config[str(i)]['daily'] = ''

            # Save the current datetime if it's new or get the old one
            now_date = int(datetime.utcnow().timestamp())
            date_leak = config.get(str(i), 'date', fallback=now_date)
            atom_date_leak = str(datetime.fromtimestamp(now_date).isoformat('T')) + 'Z'
            rss_date_leak = datetime.fromtimestamp(now_date).strftime('%a, %e %b %Y %H:%M:%SZ')

            # Save this in cache
            config[str(i)]['title'] = title
            config[str(i)]['show'] = show
            config[str(i)]['desc'] = desc.replace('%', '&#37;')
            config[str(i)]['airdate'] = air_date
            config[str(i)]['date'] = str(date_leak)
            config[str(i)]['atomdateleak'] = atom_date_leak
            config[str(i)]['rssdateleak'] = rss_date_leak

            # We're now going to scan CN
            screenshots = []
            previews = []
            screens = meta.findAll('Video_Thumb_1280_720')

            for screenshot in screens:
                url = 'http://i.cdn.turner.com/v5cache/CARTOON/site/%s' % screenshot.srcUrl.string
                local_filename = screenshot.srcUrl.string.replace('/', '-')[7:]
                full_path = os.path.join(LEAK_DIR, local_filename)
                try:
                    with urllib.request.urlopen(url) as remotefile:
                        if show == 'Steven Universe':
                            if not os.path.isfile(full_path):
                                # Download it if we don't have it yet
                                with open(full_path, 'wb') as localfile:
                                    data = remotefile.read()
                                    localfile.write(data)

                                # Add a 2x2 semi-opaque white square near the CN logo to "watermark" our pics
                                try:
                                    im = Image.open(full_path)
                                    w, h = im.size
                                    ws = w - 55
                                    hs = h - 50
                                    draw = ImageDraw.Draw(im, mode='RGBA')
                                    draw.rectangle([(ws, hs), (ws + 2, hs + 2)], fill=(255, 255, 255, 200))
                                    im.save(full_path, 'JPEG', quality=99, progressive=True)
                                    del draw
                                except:
                                    print(crayons.red('Error when editing image ' + local_filename))

                    # Add our file to the list
                    if show != 'Steven Universe' and url not in screenshots:
                        screenshots.append(url.replace('http:', 'https:'))
                    elif show == 'Steven Universe' and '/leaks/' + local_filename not in screenshots:
                        screenshots.append('/leaks/' + local_filename)

                except urllib.error.HTTPError:
                    # HTTPError? I don't care
                    pass
                except:
                    # Report us if something goes wrong
                    if pb:
                        pb.push_note('LEAKBOT has a problem!',
                                     'Error when downloading a leak screenshot (' + local_filename + ')')
                    else:
                        print(crayons.red('\nError when downloading a leak screenshot (' + local_filename + ')'))

            try:
                # Filename
                filename = careid + '_B13R1.mp4'
                # Complete URL
                url = 'http://ht.cdn.turner.com/toon/big/preview/tve/' + filename
                with urllib.request.urlopen(url) as remotefile:
                    if remotefile.getcode() == 200:
                        # If file exist
                        if show == 'Steven Universe':
                            if not os.path.isfile(os.path.join(LEAK_DIR, filename)):
                                # Download it if we don't have it yet
                                with open(os.path.join(LEAK_DIR, filename), 'wb') as localfile:
                                    data = remotefile.read()
                                    localfile.write(data)
                            # Add our file to the list
                            previews.append('/leaks/' + filename)
                        else:
                            previews.append(url.replace('http:', 'https:'))
            except:
                # If something goes wrong, it's probably normal
                pass

            try:
                # Filename
                filename = careid + '_3500kbps_1280x720.mp4'
                # Complete URL
                testurl = 'http://ht.cdn.turner.com/toon/big/preview/tve/' + filename
                with urllib.request.urlopen(testurl) as remotefile:
                    if remotefile.getcode() == 200:
                        # If file exist
                        if show == 'Steven Universe':
                            if not os.path.isfile(os.path.join(LEAK_DIR, filename)):
                                # Download it if we don't have it yet
                                with open(os.path.join(LEAK_DIR, filename), 'wb') as localfile:
                                    data = remotefile.read()
                                    localfile.write(data)
                            # Add our file to the list
                            previews.append('/leaks/' + filename)
                        else:
                            previews.append(url.replace('http:', 'https:'))
            except:
                # If something goes wrong, it's probably normal
                pass

            # Remove duplicates
            screenshots = list(set(screenshots))
            # Order screenshot list
            screenshots.sort()

            # Save the lists in our cache
            if len(screenshots) > 0:
                config[str(i)]['screenshots'] = ','.join(screenshots)
            if len(previews) > 0:
                config[str(i)]['previews'] = ','.join(previews)

    for section in config.sections():
        # Now, we're going to read our cache
        if section != 'DEFAULT' and section != 'history':
            # If the episode aired, we can ignore it
            dt_air = datetime.strptime(config.get(section, 'airdate'), '%Y-%m-%d %H:%M:%S')
            airing = timezone('US/Eastern').localize(dt_air)
            if airing.timestamp() < datetime.now().timestamp():
                if str(i) not in config['DEFAULT']['filter']:  # Don't add again if it's already there
                    config['DEFAULT']['filter'] += ',' + str(i)
                config.remove_section(section)
                continue

            # Add to the list to display it
            if config.get(section, 'show') != 'Steven Universe':
                otherleaks.append({
                    'contentid': section,
                    'title': config.get(section, 'title'),
                    'show': config.get(section, 'show'),
                    'desc': config.get(section, 'desc'),
                    'airdate': config.get(section, 'airdate'),
                    'date': config.get(section, 'date'),
                    'atomdate': config.get(section, 'atomdateleak'),
                    'rssdate': config.get(section, 'rssdateleak'),
                    'screenshots': config.get(section, 'screenshots', fallback='').split(','),
                    'previews': config.get(section, 'previews', fallback='').split(','),
                    'daily': None
                })
            else:
                leaks.append({
                    'contentid': section,
                    'title': config.get(section, 'title'),
                    'show': config.get(section, 'show'),
                    'desc': config.get(section, 'desc'),
                    'airdate': config.get(section, 'airdate'),
                    'date': config.get(section, 'date'),
                    'atomdate': config.get(section, 'atomdateleak'),
                    'rssdate': config.get(section, 'rssdateleak'),
                    'screenshots': config.get(section, 'screenshots', fallback='').split(','),
                    'previews': config.get(section, 'previews', fallback='').split(','),
                    'daily': config.get(section, 'daily')
                })

    print('\nLoading templates')
    # Current time
    date_gen = datetime.utcnow().strftime('%B %d %Y at %H:%M:%S')
    atom_now = datetime.utcnow().isoformat('T') + 'Z'

    # Load the environment for the templates
    j2_html = Environment(loader=FileSystemLoader(os.path.join(ROOT_DIR, 'templates', 'html')), trim_blocks=True)
    j2_xml = Environment(loader=FileSystemLoader(os.path.join(ROOT_DIR, 'templates', 'xml')), trim_blocks=True)

    print('Generating files')
    try:
        # Generate page (to a tmp folder first, in case of errors)
        j2_html.get_template('leaks.html').stream(
            pagetype='page-leaks', pagename='LEAKBOT', pagedesc='WHERE DA LEAKS AT?', dategen=date_gen,
            leaks=leaks, otherleaks=otherleaks, founds=len(leaks), otherfounds=len(otherleaks))\
            .dump(os.path.join(THIS_DIR, 'tmp', 'leaks.html'))
        # Copy from tmp to the public folder
        if len(sys.argv) <= 1 or (len(sys.argv) > 1 and sys.argv[1] != 'remote'):
            shutil.copy2(os.path.join(THIS_DIR, 'tmp', 'leaks.html'), os.path.join(PUBLIC_DIR))
    except Exception as e:
        if pb:
            pb.push_note('LEAKBOT has a problem!',
                         'Error when generating HTML: ' + str(e))
        print(crayons.red('\nError when generating HTML'))

    try:
        # Generate feeds (to a tmp folder first, in case of errors)
        j2_xml.get_template('leaks-atom.xml').stream(leaks=reversed(leaks), lastupdate=atom_now)\
            .dump(os.path.join(THIS_DIR, 'tmp', 'leaks.xml'))
        j2_xml.get_template('leaks-rss.xml').stream(leaks=reversed(leaks))\
            .dump(os.path.join(THIS_DIR, 'tmp', 'leaks.rss'))
        # Copy from tmp to the public folder
        if len(sys.argv) <= 1 or (len(sys.argv) > 1 and sys.argv[1] != 'remote'):
            shutil.copy2(os.path.join(THIS_DIR, 'tmp', 'leaks.xml'), os.path.join(PUBLIC_DIR))
            shutil.copy2(os.path.join(THIS_DIR, 'tmp', 'leaks.rss'), os.path.join(PUBLIC_DIR))
    except:
        print(crayons.red('\nError when generating Atom/RSS feeds'))

    api = []
    try:
        pst = pytz.timezone('US/Eastern')
        # Go through every leaks we found
        for leak in leaks:
            if leak['airdate']:
                air_date = pst.localize(datetime.strptime(leak['airdate'], '%Y-%m-%d %H:%M:%S')).timestamp()
            else:
                air_date = 0

            images = []
            for screen in leak['screenshots']:
                images.append({
                    'url': 'https://sug.rocks' + screen
                })

            videos = []
            for video in leak['previews']:
                videos.append({
                    'url': 'https://sug.rocks' + video
                })
            # Add to the API array output
            api.append({
                'air_date': int(air_date),
                'date': int(leak['date']),
                'desc': leak['desc'],
                'id': int(leak['contentid']),
                'images': images,
                'title': leak['title'],
                'videos': videos
            })

        # Save to json
        path = os.path.join(ROOT_DIR, 'api', 'leaks.json')

        with open(path, 'w') as f:
            f.write(json.dumps(api, indent=2, sort_keys=True))
    except:
        print(crayons.red('\nError when generating API endpoint'))

    try:
        full_api = []
        pst = pytz.timezone('US/Eastern')
        all_leaks = leaks + otherleaks

        # Go through every leaks we found
        for leak in all_leaks:
            if leak['airdate']:
                air_date = pst.localize(datetime.strptime(leak['airdate'], '%Y-%m-%d %H:%M:%S')).timestamp()
            else:
                air_date = 0

            uprefix = ''
            if leak['show'] == 'Steven Universe':
                uprefix = 'https://sug.rocks'

            images = []
            for screen in leak['screenshots']:
                images.append({
                    'url': uprefix + screen
                })

            videos = []
            for video in leak['previews']:
                videos.append({
                    'url': uprefix + video
                })

            # Add to the API array output
            full_api.append({
                'air_date': int(air_date),
                'date': int(leak['date']),
                'desc': leak['desc'],
                'id': int(leak['contentid']),
                'images': images,
                'show': leak['show'],
                'title': leak['title'],
                'videos': videos
            })

        # Save to json
        full_path = os.path.join(ROOT_DIR, 'api', 'all-leaks.json')

        with open(full_path, 'w') as f:
            f.write(json.dumps(full_api, indent=2, sort_keys=True))
    except:
        print(crayons.red('\nError when generating Full API endpoint'))

    # Upload
    try:
        subprocess.call(['rsync', '-azP', '--delete-after', '/root/sug/public/leaks/', 'root@Starlight:/mnt/data/sug/public/leaks'])
    except:
        print(crayons.red('\nError when copying the files'))

    if len(push_queue) > 0 and pb:
        # Send leaks to pushbullet subscribers
        for item in push_queue:
            print(crayons.blue('\nPushing: ' + item))
            pb.push_link('LEAKBOT found something! "' + item + '"', 'https://sug.rocks/leaks.html',
                         channel=pb.get_channel('sugleaks'))

    # LEAKBOT will go on and continue from last found id
    print(crayons.blue('idstart was updated from ' + str(id_start + gobackabit) + ' to ' + config['DEFAULT']['idstart']))

    # Save our config/cache
    with open(os.path.join(THIS_DIR, 'config.ini'), 'w') as cache:
        config.write(cache)

    print(crayons.green('Done.'))

    # Print current date and time for debug
    print(crayons.blue(datetime.now()))


if __name__ == '__main__':
    # Start our search as a subprocess
    p = multiprocessing.Process(target=get_leaks)
    p.start()

    # Run for 1h30 or until it ends
    p.join(5400)

    # If thread is still alive
    if p.is_alive():
        print(crayons.red('\nSearch didn\'t end in time, kill the process...'))
        # Terminate
        p.terminate()
        p.join()
